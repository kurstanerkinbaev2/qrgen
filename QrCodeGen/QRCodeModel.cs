﻿namespace QrCodeGen
{
    public class QRCodeModel
    {
        public string qr_version { get; set; }
        public string qr_type { get; set; }
        public string qr_merchant_provider { get; set; }
        public string qr_service_id { get; set; }
        public string qr_account { get; set; }
        public int qr_mcc { get; set; }
        public string qr_ccy { get; set; }
        public string qr_transaction_id { get; set; }
        public string qr_comment { get; set; }
        public string qr_control_sum { get; set; }
        public int? amount { get; set; }
    }
}
