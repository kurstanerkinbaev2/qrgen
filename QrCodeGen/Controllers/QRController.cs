﻿using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
namespace QrCodeGen.Controllers
{
    [ApiController]
    [Route("/qr-code")]
    public class QRController : Controller
    {
        [HttpGet("generate")]
        public IActionResult GenerateQR([FromQuery] int? amount)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(QRDataToJson(amount), QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20, Color.Black, Color.White, true);

            Bitmap logo = new Bitmap("C:\\Users\\kerkinbaev\\Downloads\\esblogo3.png");

            int enlargedLogoSize = logo.Width + 30;

            Bitmap logoWithCircularBorder = new Bitmap(enlargedLogoSize, enlargedLogoSize);
            using (Graphics graphics = Graphics.FromImage(logoWithCircularBorder))
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;

                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddEllipse(new Rectangle(0, 0, enlargedLogoSize, enlargedLogoSize));
                    graphics.FillPath(Brushes.White, path);
                }

                graphics.DrawImage(logo, (enlargedLogoSize - logo.Width) / 2, (enlargedLogoSize - logo.Height) / 2);
            }

            int qrCodeCenterX = qrCodeImage.Width / 2 - logoWithCircularBorder.Width / 2;
            int qrCodeCenterY = qrCodeImage.Height / 2 - logoWithCircularBorder.Height / 2;

            using (Graphics graphics = Graphics.FromImage(qrCodeImage))
            {
                graphics.DrawImage(logoWithCircularBorder, qrCodeCenterX, qrCodeCenterY);
            }

            var bitmapBytes = BitmapToBytes(qrCodeImage);

            return File(bitmapBytes, "image/jpeg");
        }
        private static byte[] BitmapToBytes(Bitmap qrCodeImage)
        {
            using(MemoryStream ms = new MemoryStream())
            {
                qrCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }
        private string QRDataToJson(int? amount)
        {
            var qrData = getQRData(amount);
            return JsonSerializer.Serialize(qrData);
        }

        private string encodQRData(int? amount)
        {
            var qrData = getQRData(amount);
            var strQRData = $"{
                qrData.qr_version 
                + qrData.qr_type 
                + qrData.qr_merchant_provider 
                + qrData.qr_service_id 
                + qrData.qr_account 
                + qrData.qr_mcc 
                + qrData.qr_ccy 
                + qrData.qr_transaction_id 
                + qrData.qr_comment 
                + qrData.amount}";

            byte [] bytes = Encoding.UTF8.GetBytes(strQRData);

            byte[] hashBytes;
            using (SHA256 sha256 = SHA256.Create())
            {
                hashBytes = sha256.ComputeHash(bytes);
            }

            string hashedString = BitConverter.ToString(hashBytes).Replace("-", "");
            return "https://pay.payqr.kg#" + hashedString;
        }

        private QRCodeModel getQRData(int? amount)
        {
            return new QRCodeModel()
            {
                qr_version = "01",
                qr_type = amount == null ? "DYNAMIC" : "STATIC",
                qr_merchant_provider = "esb.kg",
                qr_service_id = "1234",
                qr_account = "1234",
                qr_mcc = 6012,
                qr_ccy = "417",
                qr_transaction_id = "1234",
                qr_comment = "comment",
                qr_control_sum = "4F2A",
                amount = amount
            };
        }
    }
}
